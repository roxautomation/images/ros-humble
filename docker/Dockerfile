FROM ros:humble-ros-base

ENV LANG en_US.UTF-8

ARG USERNAME=dev
ARG UID=1000
ARG GID=1000

ENV DEBIAN_FRONTEND=noninteractive

ENV ROS_DISTRO="humble"
ENV SHELL=/bin/bash

# Check for mandatory build arguments
RUN : "${UID:?Build argument needs to be set and non-empty.}" && \
    : "${GID:?Build argument needs to be set and non-empty.}"

# Create the user
RUN groupadd --gid $GID $USERNAME \
    && useradd --uid $UID --gid $GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# dialout group
RUN usermod -a -G dialout $USERNAME

# upgrade system
RUN apt-get update && \
    apt-get upgrade -y

# install packages
RUN apt-get install -y \
    ssh \
    python3-pip \
    vim \
    locales \
    curl

# set locale
RUN locale-gen en_US en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG en_US.UTF-8

# setup ssh folder
RUN mkdir -p /home/$USERNAME/.ssh
RUN chown -R $USERNAME:$USERNAME /home/$USERNAME/.ssh
RUN echo "Host *.trabe.io\n\tStrictHostKeyChecking no\n" >> /home/$USERNAME/.ssh/config

# make python3 default
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# update pip
RUN python -m pip install --upgrade pip

# create workspace
RUN mkdir /workspaces \
    && chown -R $USERNAME:$USERNAME /workspaces

# create vscode extensions space
# see https://code.visualstudio.com/remote/advancedcontainers/avoid-extension-reinstalls

RUN mkdir -p /home/$USERNAME/.vscode-server/extensions \
    && chown -R $USERNAME /home/$USERNAME/.vscode-server

USER ${USERNAME}

# Some ROS environment variables
ENV ROS_DISTRO="humble"
ENV ROS_PYTHON_VERSION="3"
ENV ROS_VERSION="2"
ENV PATH="/home/$USERNAME/.local/bin:${PATH}"

# add convenience functions
RUN echo 'alias ros_init="source /opt/ros/$ROS_DISTRO/setup.bash"' >> ~/.bashrc
RUN echo 'export PS1="🐭  \[\033[1;36m\]\h \[\e[33m\]\W\[\e[m\] \[\033[1;36m\]# \[\033[0m\]"' >> ~/.bashrc

# source ros in every terminal
RUN echo 'source /opt/ros/$ROS_DISTRO/setup.bash' >> ~/.bashrc
